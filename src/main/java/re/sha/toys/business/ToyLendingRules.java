package re.sha.toys.business;

import re.sha.business.basic.BasicLendingRules;
import re.sha.exceptions.NoHoldingException;
import re.sha.exceptions.ShareException;
import re.sha.exceptions.TooManyItemsException;
import re.sha.model.CatalogueEntry;
import re.sha.model.LendingRecord;
import re.sha.model.Model;
import re.sha.model.User;
import re.sha.toys.model.jpa.ToyCatalogueEntry;
import re.sha.toys.model.jpa.ToyModel;
import re.sha.toys.model.jpa.ToyUser;

public class ToyLendingRules extends BasicLendingRules {

  // version of the model that is already the appropriate type, set in setModel(Model)
  ToyModel model = null; 

  @Override
  public LendingRecord borrow(User user, CatalogueEntry entry)
      throws NoHoldingException, TooManyItemsException, ShareException {
     
    ToyUser toyUser = null;
    ToyCatalogueEntry toyEntry = null;
    
    if(user instanceof ToyUser) {
      toyUser = (ToyUser) user;
    } else {
      throw new RuntimeException("got a User that is not a ToyUser!");
    }
    
    if(entry instanceof ToyCatalogueEntry) {
      toyEntry = (ToyCatalogueEntry)entry; 
    } else {
      throw new RuntimeException("got a CatalogueEntry that is not a ToyCatalogueEntry!");
    }
    
    int childAge = toyUser.getChildAge();
    if(childAge < toyEntry.getMinAge()) {
      throw new ShareException("The toy is not appropriate for your child!");
    }
   
    LendingRecord lr = super.borrow(user, entry);
    return lr;    
  }

  @Override
  public void setModel(Model model) {
    if(model instanceof ToyModel) {
      this.model = (ToyModel) model;
      super.model = model;
    } else {
      throw new RuntimeException("Got a model that is not a ToyModel!");
    }
  }

}
