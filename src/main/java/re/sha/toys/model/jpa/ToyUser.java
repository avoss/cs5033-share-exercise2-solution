package re.sha.toys.model.jpa;

import java.time.LocalDate;

import javax.mail.internet.AddressException;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.google.gson.annotations.Expose;

import re.sha.model.jpa.JpaUser;

@Entity
@DiscriminatorValue("T")
public class ToyUser extends JpaUser {
  
  @Expose
  private LocalDate birthdayOfChild = null;
  
  /**
   * For Hibernate.
   */
  ToyUser() {
    super();
  }

  /**
   * A Toy Share User needs a title, name, surname, email as well as the birthday of their child.
   */
  ToyUser(String title, String name, String surname, String email, LocalDate boc) throws AddressException {
    super(title, name, surname, email);
    this.birthdayOfChild = boc;
  }
  
  public int getChildAge() {
    LocalDate now = LocalDate.now();
    int age = this.birthdayOfChild.until(now).getYears();
    return age;    
  }

}
