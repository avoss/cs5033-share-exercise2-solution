package re.sha.toys.model.jpa;

import javax.persistence.EntityManager;

import bitronix.tm.BitronixTransactionManager;
import bitronix.tm.TransactionManagerServices;
import re.sha.model.Model;
import re.sha.model.jpa.JpaModel;
import re.sha.model.jpa.JpaModelFactory;

/**
 * Extension of the {@link JpaModelFactory} to ensure we get {@link ToyModel} instances
 * instead of vanilla {@link JpaModel}.
 * 
 * @author alex.voss@st-andrews.ac.uk
 */
public class ToyModelFactory extends JpaModelFactory {

  @Override
  public Model provide() {
    BitronixTransactionManager transactionManager =
        TransactionManagerServices.getTransactionManager();
    EntityManager entityManager = emf.createEntityManager();
    return new ToyModel(entityManager, transactionManager);
  }

}
