package re.sha.toys.model.jpa;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.mail.internet.AddressException;
import javax.persistence.EntityManager;

import bitronix.tm.BitronixTransactionManager;
import re.sha.model.CatalogueEntry;
import re.sha.model.User;
import re.sha.model.jpa.JpaModel;

public class ToyModel extends JpaModel {
  
  private final DateTimeFormatter dateFormatter = DateTimeFormatter.ISO_DATE;

  public ToyModel(EntityManager entityManager, BitronixTransactionManager transactionManager) {
    super(entityManager, transactionManager);
  }

  /**
   * Create a new {@link ToyCatalogueEntry} given the EAN and the minimum suitable age for the
   * product.
   */
  public CatalogueEntry createToyEntry(String ean, int minAge) {
    ToyCatalogueEntry entry = new ToyCatalogueEntry(ean, minAge);
    this.em.persist(entry);
    return entry;
  }

  /**
   * Given its ID, find a {@link ToyCatalogueEntry}.
   */
  public ToyCatalogueEntry getToyCatalogueEntryById(long id) {
    return this.em.find(ToyCatalogueEntry.class, id);
  }

  /**
   * Create a new {@link Toyuser}.
   *
   */
  public User createToyUser(String title, String name, String surname, String email, String cdob)
      throws AddressException {
        
    LocalDate childDob = LocalDate.parse(cdob, this.dateFormatter);
    ToyUser user = new ToyUser(title, name, surname, email, childDob);
    this.em.persist(user);
    return user;
  }

}
