package re.sha.toys.model.jpa;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Lob;

import com.google.gson.annotations.Expose;

import re.sha.model.jpa.JpaCatalogueEntry;

@Entity
@DiscriminatorValue("T")
public class ToyCatalogueEntry extends JpaCatalogueEntry {
  
  @Expose  
  private int minAge = 99;
  
  @Expose
  private String imageFilename = null;
  
  @Expose
  private String imageType = null;
  
  @Lob
  @Column(length = 500000) // 500K size limit
  private byte[] image = null;
 
  /**
   * For Hibernate only.
   */
  ToyCatalogueEntry() {
    super();
  }

  /**
   * A ToyCatalogueEntry requires an EAN number and a minimum age. 
   * https://en.wikipedia.org/wiki/International_Article_Number_(EAN)
   */
  ToyCatalogueEntry(String ean, int minAge) {
    super("ean", ean);
  }

  public int getMinAge() {
    return this.minAge;
  }
  
  public void setImageFilename(String filename) {
    this.imageFilename = filename;
  }
  
  public String getImageFilename() {
    return this.imageFilename;
  }
  
  public void setImageType(String type) {
    this.imageType = type;
  }
  
  public String getImageType() {
    return this.imageType;
  }
  
  public void setImageData(byte[] image) {
    this.image = image;
  }
  
  public byte[] getImageData() {
    return this.image;
  }
  
}
