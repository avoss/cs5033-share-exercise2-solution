package re.sha.toys.rest;

import javax.mail.internet.AddressException;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import re.sha.exceptions.ShareException;
import re.sha.model.User;
import re.sha.rest.BaseApi;
import re.sha.toys.model.jpa.ToyModel;

/**
 * Implements a RESTful API for users of the Toy Share product.
 * 
 * We could use an instance of UserApi here and set up forwarding method calls
 * that make use of its functionality while responding to URLs with the /toyuser path element.
 * Or, we simply use the endpoints under /user for some of the operations we need. It's
 * easier to go with the latter for now.
 * 
 * @author alex.voss@St-andrews.ac.uk
 */
@Path("/toyuser")
public class ToyUserApi extends BaseApi {
  
  // this is set in postConstruct()
  private ToyModel toyModel = null;
  
  @PUT
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{title}/{name}/{surname}/{email}/{cdob}")
  public final Response createUser(
      @PathParam("title") final String title,
      @PathParam("name") final String name,
      @PathParam("surname") final String surname,
      @PathParam("email") final String email,
      @PathParam("cdob") final String cdob
  ) throws AddressException, ShareException {
    try {
      this.model.beginTransaction();
      User user = this.toyModel.createToyUser(title, name, surname, email, cdob);
      String data = gson.toJson(user) + "\n";
      this.model.commitTransaction();
      this.model.close();
      return Response.ok(data).build(); 
    } catch(Exception e) {
      this.model.rollbackTransaction();
      this.model.close();
      throw e;
    }    
  }

  
  @Override
  public void postConstruct() {
    super.postConstruct();
    if (this.model instanceof ToyModel) {
      this.toyModel = (ToyModel) this.model;
    } else {
      throw new RuntimeException("got a Model that is not a ToyModel!");
    }
  }
}
