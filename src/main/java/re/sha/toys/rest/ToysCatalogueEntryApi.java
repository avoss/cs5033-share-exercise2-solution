package re.sha.toys.rest;

import java.io.IOException;
import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import re.sha.exceptions.ShareException;
import re.sha.model.CatalogueEntry;
import re.sha.rest.BaseApi;
import re.sha.rest.except.ApiNotFoundException;
import re.sha.toys.model.jpa.ToyCatalogueEntry;
import re.sha.toys.model.jpa.ToyModel;

/**
 * Implements RESTful API for toys. Note that the REST API for {@link CatalogueEntry} can stay in
 * place and continue to be used. The endpoints here are simply more specific and allow the handling
 * of the images.
 * 
 * The code for handling images is adapted from:
 * https://examples.javacodegeeks.com/enterprise-java/rest/jersey/jersey-file-upload-example/
 * 
 * We could use an instance of CatalogueEntryApi here and set up forwarding method calls
 * that make use of its functionality while responding to URLs with the /toy path element.
 * Or, we simply use the endpoints under /entry for some of the operations we need. It's
 * easier to go with the latter for now.
 * 
 * @author alex.voss@st-andrews.ac.uk
 */
@Path("/toy")
public class ToysCatalogueEntryApi extends BaseApi {

  // this is set in postConstruct()
  private ToyModel toyModel = null;

  @PUT
  @Path("{ean}/{minAge}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response createNewToyEntry(@PathParam("ean") String ean, @PathParam("minAge") int minAge)
      throws ShareException {
    try {
      this.model.beginTransaction();
      CatalogueEntry entry = this.toyModel.createToyEntry(ean, minAge);
      String data = gson.toJson(entry) + "\n";
      this.model.commitTransaction();
      this.model.close();
      return Response.ok(data).build();
    } catch (Exception e) {
      this.model.rollbackTransaction();
      this.model.close();
      throw e;
    }
  }

  @POST
  @Path("{id}/image")
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  @Produces(MediaType.APPLICATION_JSON)  
  public Response setImage(
      @PathParam("id") long id,
      @FormDataParam("file") InputStream is,
      @FormDataParam("file") FormDataContentDisposition contentDispoHeader) throws Exception {
    
    // TODO: the type of the image is wrong, should use this instead:
    // http://stackoverflow.com/questions/4157127/how-to-get-mime-type-of-uploaded-file-in-jersey 
    
    try {
      this.model.beginTransaction();
      ToyCatalogueEntry entry = this.toyModel.getToyCatalogueEntryById(id);  
      if(entry == null) {
        throw new ApiNotFoundException("could not find the catalogue entry!");
      }
      entry.setImageFilename(contentDispoHeader.getFileName());
      //entry.setImageType(contentDispoHeader.getType());
      byte[] buffer = IOUtils.toByteArray(is);
      entry.setImageData(buffer);
      String data = gson.toJson(entry) + "\n";
      this.model.commitTransaction();
      this.model.close();
      return Response.ok(data).build();
    } catch (Exception e) {
      this.model.rollbackTransaction();
      this.model.close();
      throw e;
    }
  }

  @GET
  @Path("{id}/image")  
  public Response getImage(@PathParam("id") long id) throws ShareException {
    try {
      this.model.beginTransaction();
      ToyCatalogueEntry entry = this.toyModel.getToyCatalogueEntryById(id);    
      if(entry == null) {
        throw new ApiNotFoundException("could not find the catalogue entry!");
      }
      byte[] data = entry.getImageData();
      if(data == null) {
        throw new ApiNotFoundException("no image stored for this catalogue entry!");
      }
      this.model.commitTransaction();
      // TODO: can we control the filename?
      return Response.ok(data).type("image/png").build();
    } catch (Exception e) {
      this.model.rollbackTransaction();
      this.model.close();
      throw e;
    }
  }

  @Override
  public void postConstruct() {
    super.postConstruct();
    if (this.model instanceof ToyModel) {
      this.toyModel = (ToyModel) this.model;
    } else {
      throw new RuntimeException("got a Model that is not a ToyModel!");
    }
  }
}
