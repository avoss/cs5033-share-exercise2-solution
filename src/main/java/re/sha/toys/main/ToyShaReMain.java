package re.sha.toys.main;

import java.io.File;
import java.io.IOException;
import java.net.URI;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import re.sha.business.LendingRules;
import re.sha.business.RecallRules;
import re.sha.business.ReservationRules;
import re.sha.business.basic.BasicLendingRules;
import re.sha.business.basic.BasicRecallRules;
import re.sha.business.basic.BasicReservationRules;
import re.sha.model.Model;
import re.sha.model.jpa.JpaModelFactory;
import re.sha.toys.business.ToyLendingRules;
import re.sha.toys.model.jpa.ToyModelFactory;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

/**
 * Main class for the Toy Share Product.
 * 
 * @author alex.voss@st-andrews.ac.uk
 */
public class ToyShaReMain {
  
  public final URI BASE_URI = URI.create("http://127.0.0.1:9998/");
  
  @Option(name = "--dbproperties", usage="name of a datasources.properties file to use for database settings")
  File dbProperties = null;

  /**
   * main method simply creates an instance and dispatches to the {@link #run()} method.
   */
  public static void main(String[] args) {
    ToyShaReMain main = new ToyShaReMain();
    try {
      main.parseArgs(args);
      main.run();
    } catch(IOException | CmdLineException  e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Parse the command-line arguments. Currently only affects the database.properties filename.
   */
  private void parseArgs(String[] args) throws CmdLineException {
    CmdLineParser parser = new CmdLineParser(this);    
    parser.parseArgument(args); 
    if(this.dbProperties != null) {
      JpaModelFactory.setDsPropsFilename(this.dbProperties.getAbsolutePath());     
    }  
  }

  /**
   * Starts the Grizzly server to handle requests and waits for the user to hit enter to terminate the server. 
   */
  private void run() throws IOException {

    final HttpServer httpServer = this.createServer();
    System.out.println("Starting grizzly2...");
    httpServer.start();
    System.out.println(String.format("Jersey app started with WADL available at "
        + "%sapplication.wadl%nHit enter to stop it...", BASE_URI));
    System.in.read();
    httpServer.shutdownNow();
  }
  
  /**
   * Create a Grizzly server and register the classes that make up this application.
   */
  private HttpServer createServer() throws IOException {
    final ResourceConfig rc = new ResourceConfig();
    rc.property(ServerProperties.TRACING, "ON_DEMAND");
    rc.packages("re.sha.toys.rest"); // add more REST API endpoints
    rc.packages("re.sha.rest");
    rc.register(MultiPartFeature.class);

    rc.register(new AbstractBinder() {
      @Override
      protected void configure() {
        bindFactory(ToyModelFactory.class).to(Model.class);  
        bind(ToyLendingRules.class).to(LendingRules.class);  // different business rules implementation here
        bind(BasicRecallRules.class).to(RecallRules.class);
        bind(BasicReservationRules.class).to(ReservationRules.class);
      }
    });
    return GrizzlyHttpServerFactory.createHttpServer(this.BASE_URI, rc);   
  }
}
